# Copyright © 2005-2009 TUBITAK/UEKAE
# Licensed under the GNU General Public License, version 2.
# See the file http://www.gnu.org/copyleft/gpl.txt.
#
# Original work belongs Gentoo Linux

CC = gcc
LD = gcc
PREFIX=/usr
SBINDIR=$(PREFIX)/sbin
MANDIR=$(PREFIX)/share/man

CFLAGS = -Wall -O2

SBIN_TARGET = start-stop-daemon
MAN_TARGET = start-stop-daemon.8

TARGET = $(SBIN_TARGET) $(MAN_TARGET)

all: $(TARGET)

start-stop-daemon: start-stop-daemon.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

install: $(TARGET)
	install -D -m 0755 $(SBIN_TARGET) $(DESTDIR)$(SBINDIR)/start-stop-daemon
	install -D -m 0644 $(MAN_TARGET) $(DESTDIR)$(MANDIR)/man8/start-stop-daemon.8

clean:
	rm -f $(TARGET)
	rm -f *.o *~ core
